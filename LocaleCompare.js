;(function () {
  'use strict';

  // LOCALE COMPARE
  // ==============

  var LocaleCompare = function () {
    this.alphabets = {
      cs: [
        'A', 'Á', 'B', 'C', 'Č', 'D', 'Ď', 'E', 'É', 'Ě', 'F', 'G', 'H', 'CH', 'I', 'Í', 'J', 'K',
        'L', 'M', 'N', 'Ň', 'O', 'Ó', 'P', 'Q', 'R', 'Ř', 'S', 'Š', 'T', 'Ť', 'U', 'Ú', 'Ů', 'V',
        'W', 'X', 'Y', 'Ý', 'Z', 'Ž',
      ],
      sk: [
        'A', 'Á', 'Ä', 'B', 'C', 'Č', 'D', 'Ď', 'DZ', 'DŽ', 'E', 'É', 'F', 'G', 'H', 'CH', 'I', 'Í',
        'J', 'K', 'L', 'Ĺ', 'Ľ', 'M', 'N', 'Ň', 'O', 'Ó', 'Ô', 'P', 'Q', 'R', 'Ŕ', 'S', 'Š', 'T',
        'Ť', 'U', 'Ú', 'V', 'W', 'X', 'Y', 'Ý', 'Z', 'Ž',
      ],
       hu: [
       'A', 'Á', 'B', 'C', 'CS', 'D', 'DZ', 'DZS', 'E', 'Ë', 'É', 'F', 'G', 'GY', 'H', 'I', 'Í',
       'J', 'K', 'L', 'LY', 'M', 'N', 'NY', 'O', 'Ó', 'Ö', 'Ő', 'P', 'Q', 'R', 'S', 'SZ', 'T',
       'TY', 'U', 'Ú', 'Ü', 'Ű', 'V', 'W', 'X', 'Y', 'Z', 'ZS',
       ],
    };
    this.numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
  };

  LocaleCompare.prototype.getAlphabet = function (alphabetName) {
    alphabetName = alphabetName.toLocaleLowerCase();

    if (this.alphabets[alphabetName] !== undefined) {
      return this.alphabets[alphabetName];
    }

    throw new Error(
      'Alphabet with name `' + alphabetName + '` does not exists in getAlphabet(alphabetName).'
    );
  };

  LocaleCompare.prototype.getSplitedValue = function (value, alphabetName) {
    var alphabet = this.getAlphabet(alphabetName);
    var maxMultiCharLetterLength = 0;
    var multiCharLetters = alphabet.filter(function (letter) {
      if (letter.length > maxMultiCharLetterLength) {
        maxMultiCharLetterLength = letter.length;
      }
      return letter.length > 1 ? letter : null;
    });



    var guessLetter = function (value, firstIndex, currentIndex) {
      if (firstIndex === currentIndex) {
        return value[currentIndex];
      }

      var substringValue = value.toLocaleUpperCase().substring(firstIndex, currentIndex + 1);

      if (multiCharLetters.indexOf(substringValue) !== -1) {
        return substringValue;
      }

      return guessLetter(value, firstIndex, currentIndex - 1);
    };

    var splitedValue = [];
    for (var letterIndex = 0; letterIndex < value.length;) {
      var letter = guessLetter(value, letterIndex, letterIndex + maxMultiCharLetterLength - 1);
      splitedValue.push(letter);
      letterIndex += letter.length;
    }

    return splitedValue;
  };

  LocaleCompare.prototype.compare = function (valueA, valueB, alphabetName) {
    var characters = this.numbers.concat(this.getAlphabet(alphabetName));

    valueA = this.getSplitedValue(valueA.toLocaleUpperCase(), alphabetName);
    valueB = this.getSplitedValue(valueB.toLocaleUpperCase(), alphabetName);

    for (var letterIndex = 0; letterIndex < Math.max(valueA.length, valueB.length); letterIndex++) {
      var indexOfValueA = characters.indexOf(valueA[letterIndex]);
      var indexOfValueB = characters.indexOf(valueB[letterIndex]);

      // Temporary solution
      // TODO: Implement special charactes (like comma, dot, ...) to comparison
      if (indexOfValueA === -1 || indexOfValueB === -1) {
        continue;
      }

      // Temporary obsolete
      if (indexOfValueA === -1) {
        throw new Error(
          'Alphabet with name `' + alphabetName + '` does not contain letter `' +
          valueA[letterIndex] + '` in compare(valueA, valueB, alphabetName).'
        );
      }

      // Temporary obsolete
      if (indexOfValueB === -1) {
        throw new Error(
          'Alphabet with name `' + alphabetName + '` does not contain letter `' +
          valueB[letterIndex] + '` in compare(valueA, valueB, alphabetName).'
        );
      }

      if (indexOfValueA > indexOfValueB || letterIndex === valueA.length) {
        return 1;
      }

      if (indexOfValueA < indexOfValueB || letterIndex === valueB.length) {
        return -1;
      }
    }

    return 0;
  };

  // console.log((new LocaleCompare()).compare('AA', 'AA', 'cs'));
  // console.log((new LocaleCompare()).compare('AÁ', 'AA', 'cs'));
  // console.log((new LocaleCompare()).compare('AA', 'AÁ', 'cs'));
  // console.log((new LocaleCompare()).getSplitedValue('Nyirokrendszer', 'hu'));
}());
