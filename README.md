LOCALE COMPARE
=

Autor: Bedřich Schindler

Stav: Ve vývoji

LocaleCompare.constructor()
-

Načte objekt s abecedami.

LocaleCompare.getAlphabet(alphabetName)
-

Podle parametru _alphabetName_ vrátí pole s velkými písmeny dané abecedy. Název abecedy je stejný jako html href-lang. Pokud je název abecedy neplatný, vyhodí výjimku.

LocaleCompare.getSplitedValue(value, alphabetName)
-

Podle parametru _alphabetName_ získá pole s velkými písmeny dané abecedy a rozdělí vstupní hodnotu parametru _value_ na jednotlivá písmena abecedy a vrátí jej jako pole. Tento krok je nezbytný pro zjištění víceznakových písmen v abecedě. Pokud je název abecedy neplatný, vyhodí výjimku.

LocaleCompare.compare(valueA, valueB, alphabetName)
-
Přijímá na vstupu dvě hodnoty a vrací hodnoty:
* -1, pokud je A < B
* 0, pokud je A = B
* 1, pokud je A > B

Tyto hodnoty je možné předat metodě Array.sort(), která seřadí pole hodnot na zákaldě pořádí písmen v abecedě, která je inicializovaná v konstruktoru.